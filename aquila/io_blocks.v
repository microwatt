// © 2017 - 2022 Raptor Engineering, LLC
//
// Released under the terms of the LGPL v3+
// See the LICENSE file for full details
//
// Generic I/O blocks required for the LPC I/O signals
// Currently implemented for the ECP5 (Trellis) only, other implementations welcome!

`define FPGA_TYPE_ECP5

module aquila_lpc_sdr_tristate(
		output wire i,
		input wire oe,
		input wire o,
		input wire clk,
		inout p
	);

	`ifdef FPGA_TYPE_ECP5

	wire buffer_i;
	reg buffer_oe = 0;
	wire buffer_o;

	always @(posedge clk) begin
		buffer_oe <= oe;
	end

	// SDR input buffer
	IFS1P3BX ib0(
		.SCLK(clk),
		.PD(0),
		.SP(1),
		.D(buffer_i),
		.Q(i)
	);

	// SDR output buffer
	OFS1P3BX ob0(
		.SCLK(clk),
		.PD(0),
		.SP(1),
		.D(o),
		.Q(buffer_o)
	);

	TRELLIS_IO #(
		.DIR("BIDIR")
	) io0(
		.B(p),
		.I(buffer_o),
		.O(buffer_i),
		.T(~buffer_oe)
	);

	`endif // FPGA_TYPE_ECP5

endmodule